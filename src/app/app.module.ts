import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router'; //add in 
import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UsersComponent } from './users/users.component';
import{AngularFire} from 'angularfire2';
import{AngularFireModule} from 'angularfire2';
import { PostFormComponent } from './post-form/post-form.component';
import { FireComponent } from './fire/fire.component';
 //import { UsersComponent } from './user/user.component';

export const firebaseConfig = {
   apiKey: "AIzaSyAmAEeNPpJbucZGe4l5iL4asmkgZNssGGg",
    authDomain: "hwproject-6cd22.firebaseapp.com",
    databaseURL: "https://hwproject-6cd22.firebaseio.com",
    storageBucket: "hwproject-6cd22.appspot.com",
    messagingSenderId: "713007716899"
}


const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'fire', component: FireComponent },
  { path: '', component: PostsComponent },
  { path: '**', component: PageNotFoundComponent }
//Add a comment to this line
];




@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UsersComponent,
    PostFormComponent,
    FireComponent
  
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    //added in on 18.12
     RouterModule,
      RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  // providers: [UsersService],  //take from UsersService
 

  // to show post use this 
 //  providers:[DemoService],  // to show post use this 
  

  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
