import { Component, OnInit } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
@Component({
  selector: 'app-fire',
  templateUrl: './fire.component.html',
  styleUrls: ['./fire.component.css']
})
export class FireComponent implements OnInit {

   posts;
//Add a comment to this line
  constructor(af:AngularFire) { 
    af.database.list('/posts').subscribe(x => {this.posts = x;
      console.log(this.posts)})
  } 

  ngOnInit() {
  }

}
